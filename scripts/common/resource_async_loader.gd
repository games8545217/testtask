class_name ResourceAsyncLoader extends RefCounted


static func load_resource(path: String, on_progress_update: Callable, on_loaded: Callable):
	var progress = 0
	ResourceLoader.load_threaded_request(path)
	
	while progress < 1:
		var got_progress = get_progress(path)
		
		if progress != got_progress:
			on_progress_update.call(progress)
			progress = got_progress
		
		await SGL.process
	
	on_loaded.call(ResourceLoader.load_threaded_get(path))

static func get_progress(path: String):
	var attributes = []
	var status = ResourceLoader.load_threaded_get_status(path, attributes)
	
	match status:
		ResourceLoader.ThreadLoadStatus.THREAD_LOAD_INVALID_RESOURCE: push_error("Invalid resource!")
		ResourceLoader.ThreadLoadStatus.THREAD_LOAD_IN_PROGRESS: return attributes[0]
		ResourceLoader.ThreadLoadStatus.THREAD_LOAD_FAILED: push_error("Loading failed!")
		ResourceLoader.ThreadLoadStatus.THREAD_LOAD_LOADED: return 1
