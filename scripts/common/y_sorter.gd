class_name YSorter extends Node


@export var _base_index: int
var _sortable: Array[Node2D]


func _process(delta):
	_clear_nulls()
	_sort()


func _sort():
	_sortable.sort_custom(func(a, b): return a.position.y < b.position.y)
	
	#var ps =[]
	#for s in _sortable:
		#ps.append(s.z_index)
	#
	#print(ps)
	
	var index = _base_index
	for node in _sortable:
		node.z_index = index
		index += 1

func _update_sortable():
	var nodes = get_tree().get_nodes_in_group("y_sortable")
	for node in nodes:
		if node is Node2D: _sortable.append(node)
		else: push_error("Only 2D nodes must be in y_sortable group!")

func _on_gameplay_loaded():
	_update_sortable()
	
func _clear_nulls():
	var array: Array[Node2D]
	array.assign(Tools.clear_nulls(_sortable))
	_sortable = array
