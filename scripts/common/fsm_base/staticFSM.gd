class_name StaticFSM extends RefCounted


signal state_changed(State)


var active : State

var _states : Dictionary


func add_state(key, state: State):
	_states[key] = state

func switch_state(key, payload=null):
	if active: active.on_exit()
	active = _states[key]
	active.on_enter(payload)
