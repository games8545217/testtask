@tool
class_name CustomYSorter extends Node


@export var _base_index: int

func _process(delta):
	_sort()


func _sort():
	var ch = get_children()
	ch.sort_custom(func(a, b): return a.position.y < b.position.y)
	
	var ps =[]
	for s in ch:
		ps.append(s.z_index)
	
	print(ps)
	
	var index = _base_index
	for node in ch:
		node.z_index = index
		index += 1
