class_name Tools

static func get_all_children(parent: Node) -> Array[Node]:
	var children : Array[Node]
	
	for child in parent.get_children():
		children.append(child)
		children.append_array(get_all_children(child))
		
	return children

static func clear_nulls(array: Array) -> Array:
	var clean_array = []
	for item in array:
		if item != null : clean_array.append(item)
	
	return clean_array

static func arrays_equal(a: Array, b: Array) -> bool:
	if a.size() != b.size(): return false
	
	for i in range(a.size()):
		if a[i] != b[i]: return false
	
	return true

static func array_difference(a: Array, b: Array):
	var a_specific = []
	for item in a:
		if item not in b:
			a_specific.append(item)
	return a_specific
