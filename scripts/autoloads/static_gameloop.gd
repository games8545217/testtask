class_name StaticGameloop extends Node


signal physics_process
signal process


func get_frame_delta() -> float: return get_process_delta_time()
func get_physics_delta() -> float: return get_physics_process_delta_time()
func get_scene_tree() -> SceneTree: return get_tree()

func _process(_delta): physics_process.emit()
func _physics_process(delta): process.emit()
