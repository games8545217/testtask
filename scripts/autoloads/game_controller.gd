class_name GameController extends Node


func get_loading_progress() -> float:
	return SL.resolve(LocationLoader).get_progress()

func enter_location(name: String, spawn_point_number: int=0):
	SL.resolve(Game).enter_location(name, spawn_point_number)

func save_hero_progress(progress: HeroProgressRES):
	SL.resolve(PersistentProgress).save_hero_progress(progress)

func save_location_progress(location_name: String, progress: LocationProgressRES):
	SL.resolve(PersistentProgress).save_location_progress(location_name, progress)

func toggle_input(toogle: bool):
	SL.resolve(InputParser).input_toggle = toogle
