class_name ServiceLocator extends Node


var _services : Dictionary


func bind(key, service):
	_services[key] = service
	
func resolve(key) -> Variant:
	return _services[key]
