class_name InterractionText extends PanelContainer


@export var _interraction_text: Label
@export var _stay_duration: float = 1
@export var _fade_duration: float = 0.5

func toast(message: String):
	_interraction_text.text = message
	_show_text()
	_fade_out()

func _show_text():
	modulate.a = 1
	
func _fade_out():
	var timer = get_tree().create_timer(_stay_duration)
	await timer.timeout
	var tween = get_tree().create_tween().set_trans(Tween.TRANS_EXPO)
	tween.tween_property(self, "modulate:a", 0, _fade_duration)
	await tween.finished
	
