class_name HeroStats extends PanelContainer


const HEALTH_DISPLAY_STRING = "%s/%s" 


var _health: int
var _max_health: int
var _keys_amount: int

@export var _hp_value_label: Label
@export var _keys_value_label: Label


func set_health(val: int):
	_health = val
	_update_text()
	
func set_max_health(val: int):
	_max_health = val
	_update_text()

func set_keys(value: int):
	_keys_amount = value
	_update_text()

func _update_text():
	_hp_value_label.text = HEALTH_DISPLAY_STRING %[_health, _max_health]
	_keys_value_label.text = str(_keys_amount)
