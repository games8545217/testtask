class_name HUD extends Control


@export var _hero_stats: HeroStats
@export var _interraction_text: InterractionText

var _hero: HeroBrain


func _physics_process(_delta):
	if _hero:_update_stats()

func bind_to_hero(hero: HeroBrain): 
	_hero = hero

func clear_binding():
	_hero = null

func toast(message: String):
	_interraction_text.toast(message)

func _update_stats():
	_hero_stats.set_health(_hero.get_health())
	_hero_stats.set_max_health(_hero.get_max_health())
	_hero_stats.set_keys(_hero.get_keys_amount())

func _on_erase():
	clear_binding()
