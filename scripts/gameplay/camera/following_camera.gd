class_name FollowingCamera extends Camera2D


var _target: Node2D


func _physics_process(_delta):
	if _target:
		position = _target.position

func set_target(target: Node2D): _target = target
func clear_target(): _target = null

func _on_erase():
	clear_target()
