class_name HeroAnimator extends AnimationTree

var _input: InputParser

func _ready():
	_input = SL.resolve(InputParser)

func _physics_process(_delta):
	set("parameters/blend_space/blend_position", _input.get_hero_movement_direction())
