class_name HeroPickUp extends Node

@export var _raycast: HeroRayCast
@export var _health: HeroHealth
@export var _keyholder: HeroKeyholder


func _ready():
	_raycast.pick_up_nearby.connect(_pick_up)


func change_max_health(delta: float):
	_health.set_max_health(_health.get_max_health()+delta)
	
func heal(amount: float): _health.heal(amount)

func hurt(amount: float): _health.hurt(amount)

func add_key(): _keyholder.add_key()


func _pick_up(item: PickUp):
	item.pick_up(self)
