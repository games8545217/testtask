class_name HeroHealth extends Node


signal died


@export var _max: int
@export var _health_value: int


func heal(amount: int):
	if amount < 0: push_error("Heal amount must be positive!")
	_set_value(_health_value + amount)

func hurt(amount: int):
	if amount < 0: push_error("Hurt amount must be positive!")
	_set_value(_health_value - amount)

func set_max_health(val: int):
	_max = val
	if val < 0: _max = 0
	_set_value(_health_value)

func _set_value(val: int):
	_health_value = clamp(val, 0, _max)
	if _health_value == 0: died.emit()

func get_health() -> int: return _health_value
func get_max_health() -> int: return _max
func force_set_value(val: int): _health_value = val
