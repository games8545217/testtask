class_name HeroMover extends Node


@export var _body: CharacterBody2D
@export var _speed: float = 100

@onready var _input: InputParser = SL.resolve(InputParser)


func _physics_process(_delta):
	_handle_movement()

func _handle_movement():
	_body.velocity = _input.get_hero_movement_direction()*_speed
	_body.move_and_slide()
