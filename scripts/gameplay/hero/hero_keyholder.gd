class_name HeroKeyholder extends Node


@export var _keys_amount: int = 0

func has_keys() -> bool: return get_keys_amount() != 0
func use_key(): _keys_amount -= 1
func add_key(): _keys_amount += 1
	
func get_keys_amount() -> int: return _keys_amount
func force_set_keys_amount(amount: int): _keys_amount = amount
