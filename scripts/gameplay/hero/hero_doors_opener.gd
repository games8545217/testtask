class_name HeroDoorsOpener extends Node


@export var _raycast: HeroRayCast
@export var _keyholder: HeroKeyholder


func _ready():
	_raycast.keyhole_nearby.connect(_try_open_door)

func _try_open_door(keyhole: KeyHole):
	if _keyholder.has_keys() && keyhole.door_state == Enums.DoorState.KEY_LOCKED:
		keyhole.accept_key()
		_keyholder.use_key()
