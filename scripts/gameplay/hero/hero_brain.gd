class_name HeroBrain extends CharacterBody2D


@export var _mover: HeroMover
@export var _animator: HeroAnimator
@export var _health: HeroHealth
@export var _key_holder: HeroKeyholder

func _ready():
	_health.died.connect(_handle_death)

func toogle_movemnt(toggle: bool):
	_mover.toggle_movement()

func get_health() -> int: return _health.get_health()
func get_max_health() -> int: return _health.get_max_health()
func get_keys_amount() -> int: return _key_holder.get_keys_amount()

func apply_progress(progress: HeroProgressRES):
	_health.force_set_value(progress.health)
	_health.set_max_health(progress.max_health)
	_key_holder.force_set_keys_amount(progress.keys_amount)

func _dump_progress() -> HeroProgressRES:
	return HeroProgressRES.new(
		_health.get_health(),
		_health.get_max_health(),
		_key_holder.get_keys_amount()
	)


func _handle_death():
	GC.toggle_input(false)
	
func _on_save():
	GC.save_hero_progress(_dump_progress())
	
