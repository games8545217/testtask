class_name HeroInterractor extends Node

@export var _raycast: HeroRayCast
@onready var _hud: HUD = SL.resolve(HUD)
@onready var _input: InputParser = SL.resolve(InputParser)


func _ready():
	_raycast.interraction_nearby.connect(_handle_interraction)

func push_toast(message: String):
	_hud.toast(message)
	
func _handle_interraction(item: Interractable):
	if _input.is_interraction_just_pressed():
		item.interract(self)

