class_name HeroRayCast extends RayCast2D


signal interraction_nearby(Interractable)
signal pick_up_nearby(PickUp)
signal keyhole_nearby(Keyhole)


@export var _length: float = 40
@onready var _input: InputParser = SL.resolve(InputParser)

func _physics_process(_delta):
	_handle_collisions()
	_turn_to_direction()

func _handle_collisions():
	var collider = get_collider()
	
	if collider is Interractable: interraction_nearby.emit(collider)
	elif collider is PickUp: pick_up_nearby.emit(collider)
	elif collider is KeyHole: keyhole_nearby.emit(collider)

func _turn_to_direction():
	if _input.is_hero_movement_pressed():
		var direction = _input.get_hero_movement_direction()
		target_position = direction*_length
