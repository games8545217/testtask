class_name KeyHole extends Area2D


signal key_accepted

var door_state: Enums.DoorState: 
	get: return _door.state
	
@export var _door: Door


func accept_key():
	if !door_state == Enums.DoorState.KEY_LOCKED: push_error("Can only open dor in KEY_LOCKED state!")
	key_accepted.emit()
