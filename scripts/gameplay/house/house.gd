class_name House extends StaticBody2D


@export var _interrior_location: String
@export var _door: Door
@export var _keyhole: KeyHole


func _ready():
	_keyhole.key_accepted.connect(_unlock_door)
	_door.hero_entered.connect(_change_location)


func key_lock_door(): _door.state = Enums.DoorState.KEY_LOCKED

func permanently_lock_door(): _door.state = Enums.DoorState.LOCKED_PERMANENTLY

func _unlock_door(): _door.state = Enums.DoorState.OPEN

func _change_location():
	GC.enter_location(_interrior_location)
