class_name HouseInterior extends Node2D


@export var _spawn_point_number: int
@export var _door: Door


func _ready():
	_door.hero_entered.connect(_exit_house)
	
	
func _exit_house():
	GC.enter_location("outside", _spawn_point_number)
