@tool
class_name DoorVisual extends Sprite2D


@export var _door: Door
@export var _open_frame: int
@export var _closed_frame: int


func _ready():
	_set_frame(_door.state)
	_door.state_changed.connect(_set_frame)

func _set_frame(state: Enums.DoorState):
	match state:
		Enums.DoorState.OPEN: frame = _open_frame
		_: frame = _closed_frame
