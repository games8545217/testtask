@tool
class_name Door extends Area2D


signal hero_entered
signal state_changed(int)


@export var state: Enums.DoorState:
	get: return state
	set(val):
		state = val
		state_changed.emit(state)

func _on_body_entered(body: Node2D):
	if state == Enums.DoorState.OPEN:
		hero_entered.emit()
