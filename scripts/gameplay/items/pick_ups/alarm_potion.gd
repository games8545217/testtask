class_name AlarmPotion extends PickUp


func pick_up(_handler: HeroPickUp):
	var doors = SGL.get_scene_tree().get_nodes_in_group("outside_door")
	for door in doors:
		if door.state == Enums.DoorState.LOCKED_PERMANENTLY: continue
		
		door.state = Enums.DoorState.KEY_LOCKED
	super.pick_up(_handler)
