class_name Orange extends PickUp

func pick_up(handler: HeroPickUp):
	handler.change_max_health(1)
	handler.heal(1)
	super.pick_up(handler)
