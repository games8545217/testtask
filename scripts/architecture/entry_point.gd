class_name EntryPoint extends Node


@export var _location_parent: Node
@export var _hero_parent: Node
@export var _hud: HUD
@export var _camera: FollowingCamera

var _game: Game


func _ready():
	_game = Game.new(_get_services())
	_game.boot_game()
	
func _get_services() -> BootstrapServices:
	return BootstrapServices.new(
		_location_parent,
		_hero_parent,
		_hud,
		_camera
	)
