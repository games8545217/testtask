class_name BootstrapState extends StateOfGame


var _services: BootstrapServices


func _init(services: BootstrapServices):
	_services = services


func on_enter(payload=null):
	_bind_input()
	_bind_gameplay()
	SL.bind(Game, _services.game)
	
func _bind_input():
	var input = PCInput.new()
	input.input_toggle = true
	SL.bind(InputParser, input)

func _bind_gameplay():
	SL.bind(LocationLoader, LocationLoader.new(_services.location_parent))
	SL.bind(HeroSpawener, HeroSpawener.new(_services.hero_parent))
	SL.bind(PersistentProgress, PersistentProgress.new())
	SL.bind(HUD, _services.hud)
	SL.bind(FollowingCamera, _services.camera)
