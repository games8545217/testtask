class_name GameplayLoadState extends StateOfGame

var _location_loader: LocationLoader
var _hero_spawner: HeroSpawener
var _persistent_progress: PersistentProgress
var _camera: FollowingCamera
var _hud: HUD


func on_enter(payload=null):
	SGL.get_scene_tree().call_group("persistent", "_on_save")
	
	var location_name: String = payload[0]
	var spawn_point_number: int = payload[1]
	
	_update_services()
	
	var loaded_location = await _setup_location(location_name)
	var hero = await _setup_hero(loaded_location, spawn_point_number)
	
	_resolve_hero_dependent(hero)
	SGL.get_scene_tree().call_group("gameplay_loaded_aware", "_on_gameplay_loaded")
	
	
func _update_services():
	_location_loader = SL.resolve(LocationLoader)
	_hero_spawner = SL.resolve(HeroSpawener)
	_persistent_progress = SL.resolve(PersistentProgress)
	_camera = SL.resolve(FollowingCamera)
	_hud = SL.resolve(HUD)

func _setup_location(location_name: String) -> LocationRES:
	_location_loader.load_location(location_name)
	var loaded_location = await _location_loader.loaded
	
	var location_persistence = _location_loader.try_get_location_persistence()
	if location_persistence:
		location_persistence.set_location_name(loaded_location.name)
		if _persistent_progress.has_location_progress(loaded_location.name):
			location_persistence.apply_progress(_persistent_progress.get_location_progress(loaded_location.name))
		
	return loaded_location

func _setup_hero(loaded_location: LocationRES, spawn_point_number: int) -> HeroBrain:
	_hero_spawner.spawn(loaded_location.spawn_points[spawn_point_number])
	var hero = await _hero_spawner.spawned
	if _persistent_progress.has_hero_progress():
		hero.apply_progress(_persistent_progress.get_hero_progress())
		
	return hero
	
func _resolve_hero_dependent(hero: HeroBrain):
	_hud.bind_to_hero(hero)
	_camera.set_target(hero)
