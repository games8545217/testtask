class_name BootstrapServices extends Resource


var location_parent: Node
var hero_parent: Node
var game: Game
var hud: HUD
var camera: FollowingCamera


func _init(location_parent: Node, hero_parent: Node, hud: HUD, camera: FollowingCamera):
	self.location_parent = location_parent
	self.hero_parent = hero_parent
	self.camera = camera
	self.hud = hud
	
func assing_game(game: Game): self.game = game 
