class_name PCInput extends InputParser


func get_hero_movement_direction() -> Vector2:
	if !input_toggle: return Vector2.ZERO
	
	return -Input.get_vector("hero_right","hero_left", "hero_back", "hero_forward")

func is_interraction_just_pressed() -> bool: 
	if !input_toggle: return false
	
	return Input.is_action_just_pressed("hero_interraction")
