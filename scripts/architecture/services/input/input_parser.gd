class_name InputParser extends RefCounted


var input_toggle: bool 


func get_hero_movement_direction() -> Vector2: return Vector2.ZERO

func is_hero_movement_pressed() -> bool: return get_hero_movement_direction() != Vector2.ZERO
func is_interraction_just_pressed() -> bool: return false
