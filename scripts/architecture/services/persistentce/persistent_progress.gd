class_name PersistentProgress extends RefCounted


var _hero_progress: HeroProgressRES
var _location_progress: Dictionary


func save_hero_progress(progress: HeroProgressRES): _hero_progress = progress
func get_hero_progress() -> HeroProgressRES: return _hero_progress
func has_hero_progress() -> bool: return _hero_progress != null

func save_location_progress(location_name: String, progress: LocationProgressRES): _location_progress[location_name] = progress
func get_location_progress(location_name: String) -> LocationProgressRES: return _location_progress[location_name]
func has_location_progress(location_name: String) -> bool: return _location_progress.has(location_name)
