class_name LocationPersistency extends Node



@export var _doors: Array[Door]
@export var _pick_ups: Array[PickUp]

var _location_name: String

func set_location_name(location_name: String): _location_name = location_name

func apply_progress(progress: LocationProgressRES):
	var door_index = 0
	for state in progress.door_states:
		_doors[door_index].state = state
		door_index += 1
	
	for pick_up in _pick_ups:
		var index = _pick_ups.find(pick_up)
		
		if index not in progress.left_pick_ups:
			pick_up.queue_free()
		
func _dump_progress() -> LocationProgressRES:
	var door_states : Array[Enums.DoorState] = []
	var left_pick_ups : Array[int] = []
	
	_doors.map(func(i): door_states.append(i.state))
	_pick_ups.map(func(i): if i != null: left_pick_ups.append(_pick_ups.find(i)))
	
	return LocationProgressRES.new(door_states, left_pick_ups)


func _on_save():
	GC.save_location_progress(_location_name ,_dump_progress())
