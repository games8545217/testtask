class_name HeroSpawener extends RefCounted


const HERO_PREFAB_PATH = "res://prefabs/gameplay/hero/hero.tscn"


signal spawned(HeroBrain)


var spawn_progress: float
var _hero_parent: Node


func _init(hero_parent: Node):
	_hero_parent = hero_parent


func spawn(position: Vector2):
	_erase_hero()
	ResourceAsyncLoader.load_resource(
		HERO_PREFAB_PATH, 
		_update_progress, 
		func(resorce): _spawn_hero_prefab(resorce, position)
	) 
	
func _update_progress(progress: float):
	spawn_progress = progress
	
func _spawn_hero_prefab(hero_res: Resource, position: Vector2):
	var hero = (hero_res as PackedScene).instantiate() as HeroBrain
	hero.position = position
	_hero_parent.add_child(hero)
	spawned.emit(hero)
	
func _erase_hero():
	_hero_parent.get_children().map(func(i): i.queue_free())
