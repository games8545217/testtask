class_name LocationLoader extends RefCounted


const LOCATIONS_PATH = "res://resources/instances/locations/"
const EXTENSION = ".tres"


signal loaded(LocationRES)


var loading_progress: float
var _parent: Node
var _loaded_location: LocationRES


func _init(location_parent: Node):
	_parent = location_parent


func load_location(name: String):
	if _loaded_location:
		_erase_current_location()
	ResourceAsyncLoader.load_resource(_full_path(name), _update_progress, _on_location_loaded)

func _on_location_loaded(loaded_res: Resource):
	_loaded_location = loaded_res as LocationRES
	var scene = _loaded_location.scene.instantiate()
	
	_parent.add_child(scene)
	
	loaded.emit(_loaded_location)

func _update_progress(progress: float):
	loading_progress = progress

func _full_path(name: String) -> String:
	return LOCATIONS_PATH + name + EXTENSION

func _erase_current_location():
	SGL.get_scene_tree().call_group("erase_aware", "_on_erase")
	_parent.get_children().map(func(i): i.queue_free())

func try_get_location_persistence():
	var children = Tools.get_all_children(_parent)
	for child in children: 
		if child is LocationPersistency: return child
	
	return null
