class_name Game extends RefCounted


const INITIAL_LOCATION_NAME = "Outside" #TODO: Create unified constants storage


var _bootstrap_services: BootstrapServices
var _fsm : StaticFSM


func _init(services: BootstrapServices):
	_bootstrap_services = services
	_bootstrap_services.assing_game(self)
	_setup_fsm()
	

func boot_game():
	_fsm.switch_state(BootstrapState)
	enter_location(INITIAL_LOCATION_NAME)

func enter_location(name: String, spawn_point_number: int=0):
	_fsm.switch_state(GameplayLoadState, [name, spawn_point_number])
	

func _setup_fsm():
	_fsm = StaticFSM.new()
	_fsm.add_state(BootstrapState, BootstrapState.new(_bootstrap_services))
	_fsm.add_state(InGameState, InGameState.new())
	_fsm.add_state(GameplayLoadState, GameplayLoadState.new())
	_fsm.add_state(PauseState, PauseState.new())
