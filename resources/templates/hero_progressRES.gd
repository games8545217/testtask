class_name HeroProgressRES extends Resource


@export var health: int
@export var max_health: int
@export var keys_amount: int


func _init(health: int, max_health: int, keys_amount: int):
	self.health = health
	self.max_health = max_health
	self.keys_amount = keys_amount
