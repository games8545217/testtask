class_name LocationProgressRES extends Resource


@export var door_states: Array[Enums.DoorState]
@export var left_pick_ups: Array[int]


func _init(door_states: Array[Enums.DoorState], left_pick_ups: Array[int]):
	self.door_states = door_states
	self.left_pick_ups = left_pick_ups
